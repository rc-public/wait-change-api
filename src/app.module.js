import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CustomerService } from './customer/customer.service';
import { CacheService } from './cache/cache.service';
import { RedisModule } from 'nestjs-redis';
import { CustomerController } from './customer/customer.controller';

@Module({
  imports: [RedisModule.register([
    { name: 'client', host: 'localhost', port: 6379 },
    { name: 'subscriber', host: 'localhost', port: 6379 },
  ])],
  controllers: [AppController, CustomerController],
  providers: [AppService, CustomerService, CacheService],
})
export class AppModule {}