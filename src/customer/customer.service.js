import { Injectable, Dependencies } from '@nestjs/common';
import { CacheService } from '../cache/cache.service'
import { last } from 'lodash'

@Injectable()
@Dependencies(CacheService)
export class CustomerService {
  constructor(cacheService) {
    this.cacheService = cacheService;
  }

  prefixKey (key) {
    return `customer:${key}`
  }

  async awaitForApproval(customerId, timeout = 30000) {
    const cacheKey = this.prefixKey(customerId)

    return new Promise(resolve => {
      const done = this.cacheService.watch(cacheKey, async ({ key: customerId, message }) => {
        console.log('this.cacheService.watch', message, cacheKey, customerId)

        const customer = await this.cacheService.get(cacheKey)

        done()
        resolve(customer)
      })
    })

  }

  async register(customer) {
    const { id } = customer
    const cacheKey = this.prefixKey(id)

    const isSet = await this.cacheService.set(cacheKey, customer)
    if (!isSet) throw new Error('Cache ERROR!')

    const approvedCustomer = await this.awaitForApproval(id)
    console.log('approved', approvedCustomer)

    return customer
  }
}
