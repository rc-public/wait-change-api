import { Test } from '@nestjs/testing';
import { CustomerController } from './customer.controller';

describe('Customer Controller', () => {
  let controller;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      controllers: [CustomerController],
    }).compile();

    controller = module.get(CustomerController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
