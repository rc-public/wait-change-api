import { Controller, Dependencies, Post, Body } from '@nestjs/common';
import { CustomerService } from './customer.service';

@Controller('customer')
@Dependencies(CustomerService)
export class CustomerController {
  constructor(customerService) {
    this.customerService = customerService;
  }

  @Post()
  async create(@Body() customer) {
    console.log('customer received \n', customer)

    const result = await this.customerService.register(customer);

    return result
  }
}
