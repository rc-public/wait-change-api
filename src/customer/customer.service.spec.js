import { Test } from '@nestjs/testing';
import { CustomerService } from './customer.service';

describe('CustomerService', () => {
  let service;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [CustomerService],
    }).compile();

    service = module.get(CustomerService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
