import { Injectable, Dependencies } from '@nestjs/common';
import { RedisService } from 'nestjs-redis';
import { promisify } from 'util'
import { isNil, isEmpty } from 'lodash'

const isNilOrEmpty = val => isNil(val) || isEmpty(val)

@Injectable()
@Dependencies(RedisService)
export class CacheService {
  constructor(redisService) {
    this.redisService = redisService

    this.client = redisService.getClient('client')
    this.subscriber = redisService.getClient('subscriber')

    const { options } = this.client
    this.client.on('connect', () => { console.log((`cache connected to ${options.host}:${options.port}`)) })
    this.client.on('error', error =>{ console.log(`cache connection error: ${error.message}`) })
  
    this.subscriber.on('connect', () => { console.log((`subscriber connected to ${options.host}:${options.port}`)) })
    this.subscriber.on('error', error =>{ console.log(`subscriber connection error: ${error.message}`) })

    this.getAsync = promisify(this.client.get).bind(this.client);
    this.setAsync = promisify(this.client.set).bind(this.client);
  }

  watch(key, onChange) {
    const keyPattern = `__keyspace@*__:${key}`
    this.subscriber.psubscribe(keyPattern)

    const handleMessage = (pattern, channel, message) => {
      onChange({ key: channel.slice(channel.indexOf(':')), message })
    }

    this.subscriber.addListener('pmessage', handleMessage)

    return () => {
      console.log('un-watching', keyPattern)
      this.subscriber.punsubscribe(keyPattern)
      this.subscriber.removeListener('pmessage', handleMessage)
    }
  }

  async get(key) {
    if (isNilOrEmpty(key)) throw new Error('chave nula')

    const result = await this.getAsync(key)

    try {
      return JSON.parse(result)
    } catch {
      return result
    }
  }

  async set(key, value) {
    if (isNilOrEmpty(key)) throw new Error('chave nula')

    let stringValue
    try {
      stringValue = JSON.stringify(value)
    } catch {
      stringValue = value
    }

    const result = await this.setAsync(key, stringValue)
    return result === 'OK'
  }
}
